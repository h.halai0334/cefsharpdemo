﻿using System;
using CefSharp.WinForms;
using System.Windows.Forms;

namespace TestBrowser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Text = "Whatsapp Web Desktop App";
            var browser = new ChromiumWebBrowser("https://web.whatsapp.com/");
            toolStripContainer1.ContentPanel.Controls.Add(browser);
            browser.IsBrowserInitializedChanged += OnIsBrowserInitializedChanged;
        }

        private void OnIsBrowserInitializedChanged(object sender, EventArgs e)
        {
            var b = ((ChromiumWebBrowser)sender);

            this.InvokeOnUiThreadIfRequired(() => b.Focus());
        }
    }
}